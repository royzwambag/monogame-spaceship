﻿using System;
using Microsoft.Xna.Framework;

namespace Spaceship
{
    public class Asteroid
    {
        public Vector2 position;
        public int speed;
        public int radius = 59;

        public Asteroid(int newSpeed, GraphicsDeviceManager graphics)
        {
            speed = newSpeed;
            Random random = new Random();
            position = new Vector2(graphics.PreferredBackBufferWidth + radius + 10, random.Next(0, graphics.PreferredBackBufferHeight));
        }
        
        public void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            position.X -= speed * deltaTime;
        }
    }
}