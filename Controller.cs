﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Spaceship
{
    public class Controller
    {
        public List<Asteroid> asteroids = new List<Asteroid>();
        private double timer = 2;
        private double maxTime = 2;
        private int nextSpeed = 220;
        public bool inGame = false;
        public double score = 0;

        public void Update(GameTime gameTime, GraphicsDeviceManager graphics)
        {
            if (inGame == false)
            {
                KeyboardState _keyboardState = Keyboard.GetState();
                if (_keyboardState.IsKeyDown(Keys.Enter))
                    newGame();
            }
            else
            {
                score += gameTime.ElapsedGameTime.TotalSeconds;
                timer -= gameTime.ElapsedGameTime.TotalSeconds;

                if (timer <= 0)
                {
                    asteroids.Add(new Asteroid(nextSpeed, graphics));
                    timer = maxTime;
                    if (maxTime > 0.5)
                        maxTime -= 0.1;

                    nextSpeed += 10;
                }
            }
        }

        private void newGame()
        {
            inGame = true;
            score = 0;
            timer = 2;
            maxTime = 2;
            nextSpeed = 220;
        }
    }
}