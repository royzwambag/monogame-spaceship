﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Spaceship
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Texture2D shipSprite;
        private Texture2D asteroidSprite;
        private Texture2D spaceSprite;
        private SpriteFont gameFont;
        private SpriteFont timerFont;

        private Ship player = new Ship();
        private Controller gameController = new Controller();
        

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            _graphics.PreferredBackBufferWidth = 1280;
            _graphics.PreferredBackBufferHeight = 720;
            _graphics.ApplyChanges();
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            shipSprite = Content.Load<Texture2D>("ship");
            spaceSprite = Content.Load<Texture2D>("space");
            asteroidSprite = Content.Load<Texture2D>("asteroid");
            
            gameFont = Content.Load<SpriteFont>("spaceFont");
            timerFont = Content.Load<SpriteFont>("timerFont");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (gameController.inGame)
                player.Update(gameTime);
            
            gameController.Update(gameTime, _graphics);

            foreach (Asteroid asteroid in gameController.asteroids)
            {
                asteroid.Update(gameTime);

                int sum = player.radius + asteroid.radius;
                if (Vector2.Distance(asteroid.position, player.position) < sum)
                {
                    gameOver();
                    break;
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            
            _spriteBatch.Draw(spaceSprite, new Vector2(0, 0), Color.White);
            _spriteBatch.Draw(shipSprite, new Vector2(player.position.X - 34, player.position.Y - 50), Color.White);
            foreach (Asteroid asteroid in gameController.asteroids)
                _spriteBatch.Draw(asteroidSprite, new Vector2(asteroid.position.X - asteroid.radius, asteroid.position.Y - asteroid.radius), Color.White);

            if (gameController.inGame == false)
            {
                string menuMessage = "Press Enter to start";
                Vector2 menuMessageSize = gameFont.MeasureString(menuMessage);
                int halfWidth = _graphics.PreferredBackBufferWidth / 2;
                int halfHeigth = _graphics.PreferredBackBufferHeight / 2;
                _spriteBatch.DrawString(gameFont, menuMessage, new Vector2(halfWidth - (menuMessageSize.X / 2), halfHeigth / 2), Color.White);
            }
            
            _spriteBatch.DrawString(timerFont, $"score: {Math.Floor(gameController.score).ToString()}", new Vector2(30, 30), Color.White);

            _spriteBatch.End();
            
            base.Draw(gameTime);
        }

        private void gameOver()
        {
            gameController.inGame = false;
            gameController.asteroids.Clear();
            player.position = Ship.defaultPosition;
        }
    }
}