﻿using System.Numerics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Vector2 = Microsoft.Xna.Framework.Vector2;

namespace Spaceship
{
    public class Ship
    {
        public static Vector2 defaultPosition = new Vector2(640, 360);
        public Vector2 position = defaultPosition;
        private const int speed = 180;
        public int radius = 30;

        public void Update(GameTime gameTime)
        {
            KeyboardState _keyboardState = Keyboard.GetState();
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float updateSpeed = speed * deltaTime;

            if (_keyboardState.IsKeyDown(Keys.Right) && position.X < 1280)
                position.X += updateSpeed;
            if (_keyboardState.IsKeyDown(Keys.Left) && position.X > 0)
                position.X -= updateSpeed;
            if (_keyboardState.IsKeyDown(Keys.Down) && position.Y < 720)
                position.Y += updateSpeed;
            if (_keyboardState.IsKeyDown(Keys.Up) && position.Y > 0)
                position.Y -= updateSpeed;
        }
    }
}